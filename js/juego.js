var message = document.getElementById('mensaje');

function showLose(){
	Utilities.stopTemporizer();
	message.innerHTML = "YOU LOSE!";
}	

function showWin(){
	Utilities.stopTemporizer();
	message.innerHTML = "YOU WIN!";
}

function temporizerFinished(){
	showLose();
}

app.controller("GameController", function(){

	this.randomNumber;
	this.total;
	this.showBotones = true;
	this.leftList;
	this.rightList;	

	this.startGame = function()
	{
		this.randomNumber = Utilities.random(5, 3);
		this.total = 100;
		this.showBotones = false;
		Utilities.startTemporizer(temporizerFinished);
		this.leftList = [];
		this.rightList = [];
		message.innerHTML = "";
	};

	function checkTotal(totalNumbers){
		if (!totalNumbers){
			showWin();
		}
	}

	this.moveNumberRight = function(){
		if (this.randomNumber == 5) {
			this.rightList.push(this.randomNumber);
			this.randomNumber = Utilities.random(5, 3);
			this.total--;	
			checkTotal(this.total);		
		}
		else{
			showLose();
			restartValues();
		}		
	}

	this.moveNumberLeft = function(){
		if (this.randomNumber == 3) {
			this.leftList.push(this.randomNumber);
			this.randomNumber = Utilities.random(5, 3);
			this.total--;	
			checkTotal(this.total);		
		}
		else{
			showLose();
		}
	}

	this.removeNumber = function(){		
		if (this.randomNumber == 4) {
			this.randomNumber = Utilities.random(5, 3);
			this.total--;	
			checkTotal(this.total);		
		}
		else{
			showLose();
		}
	}
	
});


	