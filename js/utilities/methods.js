(function () {	
	Utilities = {
		minutes: 1,
		seconds: 30,
		myInterval: "",

		init: function(){
		},

		random: function (max, min){
			return Math.round((Math.random() * (max-min)) + min);			
		},

		startTemporizer: function(callback){

			if (this.myInterval){
				clearInterval(this.myInterval);
			}
			var minutesX = this.minutes;
			var secondsX = this.seconds;
			var timer = document.getElementById('timer');			
			timer.value = (minutesX.toString().length == 1 ? "0" + minutesX:minutesX) + ":" + (secondsX.toString().length == 1 ? "0" + secondsX:secondsX);

			this.myInterval = setInterval(function(){

				if (!secondsX) {
					if (!minutesX){
						callback();
					}
					else
					{
						minutesX--;
						secondsX = 59;
					}				
				}
				else
					secondsX--;			
				timer.value = (minutesX.toString().length == 1 ? "0" + minutesX:minutesX) + ":" + (secondsX.toString().length == 1 ? "0" + secondsX:secondsX);

			}, 1000);
		},

		stopTemporizer: function(){
			clearInterval(this.myInterval);
		}
	}
})();